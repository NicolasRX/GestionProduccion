@extends('layouts.app')

@section('content')

    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <div class="panel panel-primary">
                    <div class="panel-heading">
                        Lista de Usuarios
                        <a href="" class="btn btn-sm btn-primary pull-right">
                            Crear usuario
                        </a>
                    </div>
                    <div class="panel-body">
                        <table class="table table-bordered table-hover">
                            <thead>
                            <tr>
                                <th width="10px">ID</th>
                                <th class="text-center">Nombre de usuario</th>
                                <th class="text-center">Email</th>
                                <th colspan="3">&nbsp;</th>
                            </tr>
                            </thead>
                            <tbody>
                               @foreach($users as $user)
                                   <tr>
                                       <td>{{ $user->id }}</td>
                                       <td>{{ $user->name }}</td>
                                       <td>{{ $user->email }}</td>
                                       <td width="10px">
                                           <a href="{{ route('users.showUser', $user->id) }}"
                                              class="btn btn-sm btn-success">
                                               Ver
                                           </a>
                                       </td>
                                       <td width="10px">
                                           <a href="{{ route('users.editUser', $user->id) }}"
                                              class="btn btn-sm btn-default">
                                               Editar
                                           </a>
                                       </td>
                                       <td width="10px">
                                           <a href="{{ route('users.editUser', $user->id) }}"
                                              class="btn btn-sm btn-default">
                                               Editar
                                           </a>
                                       </td>
                                       <td width="10px">
                                           {!! Form::open(['route' => ['users.destroy', $user->id],
                                           'method' => 'DELETE']) !!}
                                           <button class="btn btn-sm btn-danger">
                                               Eiminar
                                           </button>
                                           {!! Form::close() !!}
                                       </td>
                                   </tr>
                               @endforeach
                            </tbody>
                        </table>
                        {{ $users->render() }}
                    </div>

                </div>
            </div>
        </div>
    </div>

@endsection